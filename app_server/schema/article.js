const joi = require('joi')

const title = joi.string().required()
const content = joi.string().required().allow('')
const cate_id = joi.number().integer().min(1).required()
const state = joi.string().valid('已发布', '草稿').required()
const pagenum = joi.number().integer().min(1).required()
const pagesize = joi.number().integer().min(1).required()
const id = joi.number().integer().min(1).required()
// const cover_img = joi.object({
//   type: joi.string().valid('image/jpeg', 'image/png').required(),
//   size: joi.number().max(1024 * 1024 * 2).required(), // 最大 2MB
// }).required()

exports.add_article_schema = {
  body: {
    title,
    content,
    cate_id,
    state
  }
}
exports.get_article_list_schema = {
  query: {
    cate_id: joi.number().integer().min(1).required().allow(''),
    state: joi.string().valid('已发布', '草稿').required().allow(''),
    pagenum,
    pagesize
  }
}
exports.delete_article_schema = {
  query: {
    id: id
  }
}
exports.get_article_by_id_schema = {
  query: {
    id: id
  }
}
exports.update_article_schema = {
  body: {
    id: id,
    title,
    cate_id,
    content,
    state
  }
}