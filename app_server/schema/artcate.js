const joi = require('joi')

// 使用joi定义验证规则
// cate_name：1-10个非空格字符
// cate_alias：1-15个大小写字母和数字组成的字符串
const cate_name = joi
  .string()
  .min(1)
  .max(10)
  .trim()
  .required()
const cate_alias = joi
  .string()
  .alphanum()
  .min(1)
  .max(15)
  .trim()
  .required()
  const id = joi.alternatives().try(
    joi.number().integer().min(1),
    joi.string().pattern(/^\d+$/)
  ).required();

// 导出
exports.add_cate_schema = {
  body: {
    cate_name,
    cate_alias
  }
}
exports.delete_cate_schema = {
  params: {
    id
  }
}

exports.update_cate_schema = {
  body: {
    id,
    cate_name,
    cate_alias
  }
}