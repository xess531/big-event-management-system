const express = require('express')
const router = express.Router()
const articleHandler = require('../router_handler/article')
const expressJoi = require('@escook/express-joi')
const { add_article_schema, get_article_list_schema, delete_article_schema, get_article_by_id_schema, update_article_schema } = require('../schema/article')
const multer = require('multer')
const path = require('path')
const upload = multer({ dest: path.join(__dirname, '../uploads') }) // 指定文件的存放路径


router.post('/add', upload.single('cover_img'), expressJoi(add_article_schema), articleHandler.addArticle) 
// upload.single()会首先查找请求体中的cover_img字段
// 如果找到了，并将其保存到dist指定的路径，并在req添加一个file属性，可以通过req.file来访问此文件
router.get('/list', expressJoi(get_article_list_schema), articleHandler.getArticleList) // 获取文章列表
router.delete('/info', expressJoi(delete_article_schema), articleHandler.deleteArticle) // 删除文章
router.get('/info', expressJoi(get_article_by_id_schema), articleHandler.getArticleById) // 根据id获取文章详情
router.put('/info', upload.single('cover_img'), expressJoi(update_article_schema), articleHandler.updateArticle) // 根据id更新文章信息

module.exports = router