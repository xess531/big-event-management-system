const express = require('express')
const router = express.Router()
const artCateHandler = require('../router_handler/artcate')
const expressJoi = require('@escook/express-joi')
const { add_cate_schema, delete_cate_schema, update_cate_schema } = require('../schema/artcate')

router.get('/list', artCateHandler.getArtCateList) // 获取-文章分类
router.post('/add', expressJoi(add_cate_schema), artCateHandler.addArtCate) // 增加-文章分类
router.delete('/del/:id', expressJoi(delete_cate_schema), artCateHandler.deleteCateById) // 删除-文章分类
router.put('/info', expressJoi(update_cate_schema), artCateHandler.updateCateById) // 更新-文章分类
// router.get('/info', artCateHandler.getArtCateById) // 获取-文章分类


module.exports = router