const express = require('express')
const router = express.Router()
const userHandler = require('../router_handler/user')
const expressJoi = require('@escook/express-joi')
const { reg_login_schema } = require('../schema/user')

router.post('/reguser', expressJoi(reg_login_schema), userHandler.regUser) // 注册
// router.post('/login', expressJoi(reg_login_schema), userHandler.login) // 登录
router.post('/login', userHandler.login)

// 默认导出
module.exports = router