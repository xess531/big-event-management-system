const express = require('express')
const router = express.Router()
const userinfoHandler = require('../router_handler/userinfo')
const expressJoi = require('@escook/express-joi')
const { update_userinfo_schema, update_password_schema, update_avatar_schema } = require('../schema/user')

router.get('/userinfo', userinfoHandler.getUserInfo) // 获取用户的基本信息
router.put('/userinfo', expressJoi(update_userinfo_schema), userinfoHandler.updateUserInfo) // 更新-用户基本信息
router.patch('/updatepwd', expressJoi(update_password_schema), userinfoHandler.updatePassword) // 更新-用户密码
router.patch('/update/avatar', expressJoi(update_avatar_schema), userinfoHandler.updateAvatar) // 更新-用户头像

// 默认导出为路由对象
module.exports = router
