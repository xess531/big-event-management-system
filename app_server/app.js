const express = require('express')
const app = express()
const config = require('./config')
const joi = require('joi')
const { expressjwt } = require('express-jwt')
const cors = require('cors') // 导入cors中间件
const userRouter = require('./router/user') // 导入用户路由模块
const userinfoRouter = require('./router/userinfo') // 导入用户信息路由模块
const artCateRouter = require('./router/artcate') // 导入文章分类路由模块
const articleRouter = require('./router/article') // 导入文章路由模块

app.use(express.json({ limit: '10mb' }))// 注册解析json的中间件
app.use(cors()) // 注册跨源资源共享中间件
app.use('/uploads', express.static('./uploads')) // 向外暴露uploads目录，使其可以被访问
app.use((req, res, next) => { // 注册优化res.send()代码的中间件
  res.cc = (err, code = 1) => {
    res.send({
      code, // 默认为1 
      message: err instanceof Error ? err.message : err // 判断 err 是否是一个 Error 对象实例。如果是，它就使用 err.message 作为消息内容；如果不是，它则直接使用 err 本身作为消息内容。
    })
  }
  next()
})
app.use(express.urlencoded({ extended: false })) // 注册解析表单数据的中间件
app.use(expressjwt({ // 注册验证token的中间件
  secret: config.jwtSecretKey,
  algorithms: ['HS256']
}).unless({ path: [/^\/api\//] }))
app.use('/api', userRouter) // 注册用户路由模块
app.use('/my', userinfoRouter) // 注册用户信息路由模块
app.use('/my/cate', artCateRouter) // 注册文章分类路由模块
app.use('/my/article', articleRouter) // 注册文章路由模块
app.use((err, req, res, next) => { // 注册错误处理中间件
  // 数据验证失败
  if (err instanceof joi.ValidationError) {
    return res.cc(err)
  }
  // 捕获身份认证失败的错误
  if (err.name === 'UnauthorizedError') {
    return res.cc('身份认证失败！')
  }
  // 其他失败
  // res.cc(err)
  console.log(err)
})

// 启动web服务器
app.listen (3007, () => {
  console.log('api server running at http://127.0.0.1:3007')
})