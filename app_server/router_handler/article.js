const db = require('../db/index')
const path = require('path')

exports.addArticle = (req, res) => {
  if (!req.file || req.file.fieldname !== 'cover_img') {
    return res.cc('文章封面是必选参数！')
  }
  const articleInfo = {
    // title, content, cate_id, state
    ...req.body,
    cover_img: path.join('/uploads', req.file.filename),
    pub_date: new Date(),
    author_id: req.auth.id
  }
  const sql = 'insert into ev_articles set ?'
  db.query(sql, articleInfo, (err, results) => {
    if (err) {
      res.cc(err)
    }
    if (results.affectedRows !== 1) {
      res.cc('添加文章失败')
    }
    res.cc('添加文章成功', 0)
  })
}
exports.getArticleList = (req, res) => {
  // 1. 实现查询逻辑
  console.log('getArticleList函数')
  console.log(req.query)
  const { pagenum, pagesize, cate_id, state } = req.query
  let sql = 'select id, title, pub_date, state, cate_id from ev_articles where 1=1'
  const params = []
  console.log(cate_id)
  if (cate_id) {
    sql += ' and cate_id=?'
    params.push(cate_id)
  }
  if (state) {
    sql += ' and state=?'
    params.push(state)
  }
  // console.log(sql)
  db.query(sql, params, async (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.length === 0) {
      console.log(results.length)
      res.send({
        code: 0,
        message: '获取文章列表成功！',
        data: results,
        total: 0
      })
      return
    }
    // 2. 实现分页
    const start = (pagenum - 1) * pagesize
    const end = pagenum * pagesize
    const pageResults = results.slice(start, end)

    // 3. 实现替换属性
    // 异步获取cate_name并更新results
    try {
      const updatedPageResults = await Promise.all(pageResults.map(async (row) => {
        const cateName = await getCateName(row.cate_id)
        const { cate_id, ...rest } = row
        return {
          ...rest,
          cate_name: cateName
        }
      }))

      // 4. 发送更新后的结果
      res.send({
        code: 0,
        message: '获取文章列表成功！',
        data: updatedPageResults,
        total: results.length
      })

    } catch (error) {
      res.cc(error)
    }
  })
}
exports.deleteArticle = (req, res) => {
  const sql = 'delete from ev_articles where id=?'
  // console.log(req.query)
  db.query(sql, req.query.id, (err, results) => {
    if (err) {
      res.cc(err)
    }
    if (results.affectedRows !== 1) {
      res.cc('删除文章失败')
    }
    res.cc('删除文章成功', 0)
  })
}
exports.getArticleById = async (req, res) => {
  const sql = 'select * from ev_articles where id=?'
  db.query(sql, [req.query.id], async (err, results) => {
    if (err) {
      res.cc(err)
    }
    if (results.length !== 1) {
      res.cc('获取文章失败')
    }
    const {id, title, content, cover_img, pub_date, state, cate_id, author_id} = results[0]
    const cate_name = await getCateName(cate_id)
    const sql2 = 'select username, nickname from ev_users where id=?'
    db.query(sql2, author_id, (err, results) => {
      if (err) {
        res.cc(err)
      }
      if (results.length !== 1) {
        res.cc('获取文章失败')
      }
      const {username, nickname} = results[0]
      res.send({
        code: 0,
        message: '获取文章成功！',
        data: {
          id,
          title,
          content,
          cover_img,
          pub_date,
          state,
          cate_id,
          author_id,
          cate_name: cate_name,
          username,
          nickname
        }
      })
    })
  })
}
exports.updateArticle = (req, res) => {
  console.log('updateArticle函数')
  console.log(req.body)
  if (!req.file || req.file.fieldname !== 'cover_img') {
    return res.cc('文章封面是必选参数！')
  }
  const {id, title, cate_id, content, state} = req.body
  const articleInfo = {
    title,
    cate_id,
    cover_img: path.join('/uploads', req.file.filename),
    content,
    state
  }

  const sql = 'update ev_articles set ? where id=?'
  db.query(sql, [articleInfo, id], (err, results) => {
    if (err) {
      res.cc(err)
    }
    if (results.affectedRows !== 1) {
      res.cc('更新文章失败')
    }
    res.cc('更新文章成功', 0)
  })
}

// 获取cate_name的辅助函数
function getCateName(cateId) {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT cate_name FROM ev_article_cate WHERE id = ?'
    db.query(sql, [cateId], (error, results) => {
      if (error) {
        reject(error)
      } else if (results.length > 0) {
        resolve(results[0].cate_name)
      } else {
        reject(new Error('Category not found'))
      }
    })
  })
}
