const db = require('../db/index')
const bcrypt = require('bcryptjs')

exports.getUserInfo = (req, res) => {
  const sql = `select id, username, nickname, email, user_pic from ev_users where id=?`
  db.query(sql, req.auth.id, (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.length !== 1) {
      return res.cc('获取用户信息失败')
    }
    res.send({
      code: 0,
      message: '获取用户信息成功',
      data: results[0]
    })
  })
}
exports.updateUserInfo = (req, res) => {
  const sql = `update ev_users set ? where id=?`
  // console.log(req.body)
  db.query(sql, [req.body, req.body.id], (err, results) => {
    if (err) {
      res.cc(err)
    }
    if (results.affectedRows !== 1) {
      res.cc('修改用户信息失败')
    }
    res.cc('修改用户信息成功', 0)
  })
}
exports.updatePassword = (req, res) => {
  console.log(req.body)
  // 检查原密码是否匹配成功
  const sql = `select * from ev_users where id=?`
  db.query(sql, req.auth.id, (err, results) => {
    // 如果sql语句执行失败
    if (err) {
      return res.cc(err)
    }
    // 如果此用户不存在或存在多个
    if (results.length !== 1) {
      return res.cc('用户不存在')
    }
    // 如果原密码匹配失败
    console.log(results[0])
    const compareResult = bcrypt.compareSync(req.body.old_pwd, results[0].password)
    if (compareResult !== true) {
      return res.cc('原密码错误')
    }
  })

  // 修改密码
  const sql2 = `update ev_users set password=? where id=?`
  db.query(sql2, [bcrypt.hashSync(req.body.new_pwd, 10), req.auth.id], (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.affectedRows !== 1) {
      return res.cc('修改密码失败')
    }
    res.cc('修改密码成功', 0)
  })
}
exports.updateAvatar = (req, res) => {
  // console.log(req.body.avatar)
  const sql = `update ev_users set user_pic=? where id=?`
  db.query(sql, [req.body.avatar, req.auth.id], (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.affectedRows !== 1) {
      return res.cc('更新头像失败')
    }
    return res.cc('更新头像成功', 0)
  })
}