const db = require('../db/index')

exports.getArtCateList = (req, res) => {
  const sql = 'select * from ev_article_cate order by id asc'
  db.query(sql, (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.length < 1) {
      return res.cc('获取文章分类失败！')
    }
    res.send({
      code: 0,
      message: '获取文章分类成功！',
      data: results
    })
    console.log(results)
  })
}
exports.addArtCate = (req, res) => {
  // 查询新增的分类名与别名是否被占用
  const sql = 'select * from ev_article_cate where cate_name=? or cate_alias=?'
  console.log(req.body)
  db.query(sql, [req.body.cate_name, req.body.cate_alias], (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.length > 0) {
      return res.cc('分类名或别名被占用')
    }
    // 新增新的文章分类
    const sql2 = 'insert into ev_article_cate set ?'
    db.query(sql2, req.body, (err, results) => {
      if (err) {
        return res.cc(err)
      }
      if (results.affectedRows !== 1) {
        return res.cc('新增文章分类失败！')
      }
      res.cc('新增文章分类成功！', 0)
    })
  })
}
exports.deleteCateById = (req, res) => {
  console.log(typeof req.params.id)
  const sql = 'delete from ev_article_cate where id=?'
  db.query(sql, req.params.id, (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.affectedRows !== 1) {
      return res.cc('删除文章分类失败！')
    }
    res.cc('删除文章分类成功！', 0)
  })
}
exports.updateCateById = (req, res) => {
  // 检查（其他行）分类名和别名是否被占用
  const sql = 'select * from ev_article_cate where id<>? and (cate_name=? or cate_alias=?)'
  db.query(sql, [,req.body.id, req.body.cate_name, req.body.cate_alias], (err, results) => {
    if (err) {
      return res.cc(err)
    }
    if (results.length > 0) {
      return res.cc('分类名或别名被占用')
    }
    // 更新分类
    const sql2 = 'update ev_article_cate set ? where id=?'
    db.query(sql2, [req.body, req.body.id], (err, results) => {
      if (err) {
        return res.cc(err)
      }
      if (results.affectedRows !== 1) {
        return res.cc('更新文章分类失败')
      }
      return res.cc('更新文章分类成功', 0)
    })
  })
}
exports.getArtCateById = (req, res) => { 
  const sql = 'select * from ev_article_cate where id=?'
  console.log(req.body)
  db.query(sql, req.body.id, (err, results) => {
    if (err) {
      res.cc(err)
    }
    if (results.length !== 1) {
      res.cc('获取文章分详情失败')
    }
    res.send({
      status: 0,
      message: '获取文章分类详情成功',
      data: results[0]
    })
  })
}
