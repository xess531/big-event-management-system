const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('../config')

exports.regUser = (req, res) => {
  const userInfo = req.body
  userInfo.password = bcrypt.hashSync(userInfo.password, 10)
  // console.log(userInfo)
  // 判断用户名是否被占用
  const db = require('../db/index')
  const sql = `select * from ev_users where username=?`
  db.query(sql, userInfo.username, (err, results) => {
    // 如果执行sql语句失败
    if (err) {
      // return res.send({status: 1, message: err.message})
      return res.cc(err)
    }
    // 如果用户名被占用 
    if (results.length > 0) {
      // return res.send({ status: 1, message: '用户名被占用，请更换其他用户名！' })
      return res.cc('用户名被占用，请更换其他用户名！')
    }
  })

  // 在数据库中插入新用户
  const sql2 = `insert into ev_users set ?`
  db.query(sql2, {username: userInfo.username, password: userInfo.password}, (err, results) => {
    // 如果sql语句执行失败
    if (err) {
      // return res.send({status: 1, message: err.message})
      return res.cc(err)
    }
    // 如果影响行数大于1
    if (results.affectRows > 1) {
      console.log(results)
      // return res.send({status: 1, message: '注册用户失败！'})
      return res.cc('注册用户失败！')
    }
    // 插入成功
    // res.send({status: 0, message: '注册成功！'})
    res.cc('注册成功！', 0)
  })
}

exports.login = (req, res) => {
  console.log(req.body)
  const userInfo = req.body
  console.log(userInfo)
  // 判断数据库中是否有这个用户
  const db = require('../db/index')
  const sql = `select * from ev_users where username=?`
  db.query(sql, userInfo.username, (err, results) => {
    // 如果sql语句执行失败
    if (err) {
      return res.cc(err)
    }
    // 如果sql语句执行成功
    if (results) {
      // 如果用户名不存在或者用户名存在多个？
      if (results.length !== 1) {
        return res.cc('登录失败')
      }
    }
    // 如果密码匹配失败
    const compareResult = bcrypt.compareSync(userInfo.password, results[0].password)
    console.log('密码匹配结果：')
    console.log(compareResult)
    if (compareResult !== true) {
      return res.cc('登录失败！')
    }
    // 登录成功！生成token字符串
    const user = {...results[0], password: '', user_pic: ''} // 剔除密码和头像值
    const tokenStr = jwt.sign(user, config.jwtSecretKey, { // 生成token字符串
      expiresIn: '10h',
    })
    res.send({
      code: 0,
      message: '登录成功！',
      token: 'Bearer ' + tokenStr // 为了方便客户端使用 Token，在服务器端直接拼接上 Bearer 的前缀
    })
  })
}
