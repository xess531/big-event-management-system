import request from '@/utils/request.js'

export const userRegisterService = ({ username, password, repassword }) => 
  request.post('/api/reg', { username, password, repassword })

export const userLoginService = ({ username, password }) =>
  request.post('/api/login', { username, password })

export const userGetInfoService = () => 
  request.get('/my/userinfo')

export const userEditInfo = (data) => 
  request.put('/my/userinfo', data)

export const userUploadAvatar = (data) =>
  request.patch('/my/update/avatar', {
    avatar: data
  })

export const userUpdatePassword = ({oldPassword, newPassword, confirmPassword}) =>
  request.patch('/my/updatepwd', {
    "old_pwd": oldPassword,
    "new_pwd": newPassword,
    "re_pwd": confirmPassword
  })
