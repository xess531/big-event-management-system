import request from '@/utils/request.js'

export const articleGetChannel = () => 
  request.get('/my/cate/list')
export const articleAddChannel = (data) => 
  request.post('/my/cate/add', data)
export const articleEditChannel = (data) =>
  request.put('/my/cate/info', data)
export const articleDelChannel = (id) => {
  return request.delete(`/my/cate/del/${id}`)
}

export const articleGetList = (params) => {
  // console.log('articleGetList函数')
  // console.log(params)
  return request.get('/my/article/list', { params })
}
export const articleAdd = (data) => 
  request.post('/my/article/add', data)
export const articleGetDetail = (id) => 
  request.get('/my/article/info', {
    params: {id}
  })
export const articleDelete = (id) =>
  request.delete('/my/article/info', {
    params: {id}
  })
export const articleEdit = (data) => {
  console.log(data)
  return request.put('/my/article/info', data)
}