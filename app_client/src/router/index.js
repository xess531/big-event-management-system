import { createRouter, createWebHistory } from 'vue-router'
import { useUserStore } from '@/stores/modules/user.js'

// 创建路由实例
// .meta.env.BASE_URL vite中的环境变量 vite.config.js中的base配置项
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/login', component: () => import('../views/login/LoginPage.vue') }, // 异步组件写法
    { 
      path: '/', 
      component: () => import('../views/layout/LayoutContainer.vue'),
      redirect: '/article/manage',
      children: [
        { 
          path: '/article/manage', 
          component: () => import('../views/article/ArticleManage.vue') 
        },
        { 
          path: '/article/channel', 
          component: () => import('../views/article/ArticleChannel.vue') 
        },
        { 
          path: '/user/avatar', 
          component: () => import('../views/user/UserAvatar.vue') 
        },
        { 
          path: '/user/password', 
          component: () => import('../views/user/UserPassword.vue') 
        },
        {
          path: '/user/profile', 
          component: () => import('../views/user/UserProfile.vue')
        }
      ]
    },
  ]
})

// 登录访问拦截
// 对于各种返回值：
// true：不拦截 false：拦截 一个地址：回到此地址
router.beforeEach((to) => {
  // console.log(to)
  const userStore = useUserStore()
  if (!userStore.token && to.path !== '/login') {
    return '/login'
  }
  return true
})

export default router
