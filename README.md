# 大事件管理系统

## 一. 简介

本项目是在黑马程序员Vue课程实战项目与Node.js课程实战项目的基础上完善而成的一个小型web项目。相关技术包括：Vue3, Pinia, Vue Router, Vite, Axios, Element-Plus, Node.js, MySQL, Express。仅供本人学习使用。技术文章链接： https://juejin.cn/post/7324155708614017078



## 二. 安装与使用

#### 项目克隆

克隆项目到本地环境:

``` bash
git clone https://gitee.com/xess531/big-event-management-system.git
```

#### 网页端（app_client)

进入网页端文件目录：

``` bash
cd [网页端文件路径]
```

网页端要求node.js环境，如未安装请先安装。使用 `pnpm` 作为包管理工具。如未安装 `pnpm`，请先进行安装：

``` bash
npm install -g pnpm
```

安装项目依赖:

``` bash
pnpm install
```

启动网页端应用:

``` bash
pnpm dev
```

#### 服务器端（app_server)

进入服务器端文件目录：

``` bash
cd [服务器端文件路径]
```

服务器端也要求node.js环境，如未安装请先安装。使用`npm`作为包管理工具。安装项目依赖：

``` bash
npm install
```

`nodemon` 是一个实用的包，用于监视项目中的文件变化，检测到文件更改它将重启服务器。如未安装，可通过如下命令**<u>全局安装</u>**：

``` bash
npm install -g nodemon
```

使用 `nodemon` 替代 `node` 来运行应用，如：

```bash
nodemon app.js
```

如果没有问题，终端应该会显示：

``` bash
[root@VM-16-12-centos ~]# cd /www/projects/app_server
[root@VM-16-12-centos app_server]# nodemon app.js
[nodemon] 3.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,cjs,json
[nodemon] starting `node app.js`
api server running at [IP地址]
```



## 三. 项目API文档

查阅完整的API文档，请访问：[大事件管理系统API文档](http://big-event-vue-api-t.itheima.net)

